void ReadFiles (const string & CourseInput, vector <Course *> & C,
		const string & PeopleInput, vector <SSUPerson *> & P);
void WriteFiles (const string & CourseOutput, const vector <Course *> & C,
		const string & PeopleOutput, const vector <SSUPerson *> & P);
