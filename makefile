test : P1.out
	./P1.out
	diff SSUPeopleInput.txt SSUPeopleOutput.txt
	diff SSUCoursesInput.txt SSUCoursesOutput.txt

P1.out : Proj1Functions.o Proj1App.o Proj1RW.o SSU.o
	g++ -o P1.out Proj1Functions.o Proj1App.o Proj1RW.o SSU.o 

Proj1Functions.o : Proj1Functions.cpp Proj1Functions.h SSU.h
	g++ -c Proj1Functions.cpp -g

Proj1App.o : Proj1App.cpp Proj1Functions.h SSU.h Proj1RW.h
	g++ -c Proj1App.cpp -g

clean : 
	rm P1.out Proj1Functions.o Proj1App.o
