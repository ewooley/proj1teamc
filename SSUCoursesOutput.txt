--------------------------------- Course ---------------------------------
Name: Intro to Linux
ID: CS198
Enrollment: 28
Location: Darwin 25
Time: H09:00-11:50
Instructor: 1199
Units: 1
--------------------------------- Course ---------------------------------
Name: Team Programming
ID: CS297
Enrollment: 12
Location: Darwin 28
Time: M15:00-16:50
Instructor: 1199
Units: 1
--------------------------------- Course ---------------------------------
Name: Programming II
ID: CS298
Enrollment: 28
Location: Darwin 107
Time: TH16:00-17:15
Instructor: 1199
Units: 3
--------------------------------- Course ---------------------------------
Name: Programming II Lab
ID: CS299
Enrollment: 28
Location: Darwin 25
Time: T09:00-11:50
Instructor: 1199
Units: 1
--------------------------------- Course ---------------------------------
Name: Object Oriented Programming
ID: CS398
Enrollment: 28
Location: Darwin 31
Time: M12:00-13:50
Instructor: 1199
Units: 3
--------------------------------- Course ---------------------------------
Name: Object Oriented Programming Lab
ID: CS399
Enrollment: 28
Location: Darwin 28
Time: W12:00-14:50
Instructor: 1199
Units: 1
