#include <iostream>
#include <vector>
//#include "SSU.h" // Included in Proj1Function.h
#include "Proj1Functions.h"
using namespace std;

void AddAPerson (vector <SSUPerson * > & p, int ID, string name, string type)
{
	string email = name+"@sonoma.edu";
	if(type == "SSUPerson")
                p.push_back(new SSUPerson(name, ID, email));
	else if(type == "Employee")
	{
		string dep;
		cout << "Enter Department: ";
		cin >> dep;
		cout << endl;
		float sal;
		cout << "Enter Salary: ";
		cin >> sal;
		cout << endl;
		string title;
                cout << "Enter Title: ";
                cin >> title;
                cout << endl;
		p.push_back(new Employee(dep, 0, sal,title, name, ID, email));
	}
	else if(type == "Athlete")
	{
		string sport;
                cout << "Enter Sport: ";
                cin >> sport;
                cout << endl;
                float sship;
                cout << "Enter Scolarship: ";
                cin >> sship;
                cout << endl;
		string major = "Undeclaired";
		vector <Course*> c;
                p.push_back(new Athlete(sport, sship, name,ID, email, major, 0.0, 0.0, 0.0 , 0.0, 0.0, c));
	}	
	else if(type == "Student")
	{
		string major = "Undeclaired";
                vector <Course*> c;
                p.push_back(new Student(major, 0.0, 0.0, 0.0 , 0.0, 0.0, c, name,ID, email));
	}
	else if(type == "Faculty")
	{
		 string dep;
                cout << "Enter Department: ";
                cin >> dep;
                cout << endl;
                float sal;
                cout << "Enter Salary: ";
                cin >> sal;
                cout << endl;
                string title;
                cout << "Enter Title: ";
                cin >> title;
                cout << endl;

		vector <Course*> c;
                p.push_back(new Faculty(false, 0.0, c, dep, sal, 0, title, name,ID,  email));
	}
	else if(type == "StudentEmployee")
	{
		 string dep;
                cout << "Enter Department: ";
                cin >> dep;
                cout << endl;
                float sal;
                cout << "Enter Salary: ";
                cin >> sal;
                cout << endl;
                string title;
                cout << "Enter Title: ";
                cin >> title;
                cout << endl;

		string major = "Undeclaired";
                vector <Course*> c;
                p.push_back(new StudentEmployee(0, major, 0, 0, 0, 0, 0, c, 0, new Employee(), dep, 0, sal, title, name, ID,  email));
	}
	else if(type == "Staff")
	{
		
                 string dep;
                cout << "Enter Department: ";
                cin >> dep;
                cout << endl;
                float sal;
                cout << "Enter Salary: ";
                cin >> sal;
                cout << endl;
                string title;
                cout << "Enter Title: ";
                cin >> title;
                cout << endl;

                p.push_back(new Staff(0, new Employee(), dep, 0, sal, title, name, ID,  email));
	}	
	
}

bool DeleteAPerson (vector <SSUPerson * > & p, vector <Course * > & c)
{
	int IDtoDelete;
	cout << "Enter the ID of the person you wish to delete: ";	
	cin >> IDtoDelete;
	cout << "Searching for Person with ID: " << IDtoDelete;
	for(int i = 0; i < p.size(); i++)
	{
		cout << ".";
		if(IDtoDelete == p[i]->GetID())
		{
			cout << endl;
			
			if(p[i]->GetClassType() > 2)
			{
				cout << p[i]->GetName() << " taught the following courses: " << endl;
				// Go through all courses and delete instructor from courses & print that course.
				for(int j = 0; j < c.size(); j++)
				{
					
					if(c[j]->GetInstructor()->GetID() == IDtoDelete)
					{
						c[j]->SetInstructor(new Employee());
						cout << c[j]->GetID() << "\t" << c[j]->GetName() << endl;
					}
				}
			}
			// Staff cannot be removed because there is no way to check if the person is
			
			else if(p[i]->GetClassType() == 4)
			{
				// Go through all employees and delete possible supervisor from staff if they are the supervisor.
				for(int j = 0; j < c.size(); j++)
				{
					Staff * tmp = dynamic_cast<Staff*>(p[j]);
					if( tmp->GetSupervisor()->GetID() == IDtoDelete)
					{
						cout << p[i]->GetName() << " supervised the following people: " << endl;
						tmp->SetSupervisor(0);
						cout << p[j]->GetID() << "\t" << p[j]->GetName()  << endl;
					}
				}
			}
			cout << "Success";
			// Just to look pretty
			cout << endl;
			// Erase the person
			p.erase(p.begin()+i);
			return true;
		}
	}
	cout << "Id Not found\n\n";
	return false;
}

void AddACourse ()
{
	cout << "Function for option c. Add a Course\n";
}

void DeleteACourse ()
{
	cout << "Function for option d. Delete a Course\n";
}

void AddAStudentToACourse ()
{
	cout << "Function for option e. Add a Student to a Course\n";
}

void DropAStudentFromACourse ()
{
	cout << "Function for option f. Drop a Student from a Course\n";
}

void ModifyAnExistingPerson ()
{
	cout << "Function for option g. Modify an Existing Student\n";
}

void PrintARosterForACourse (Course * & c, vector <SSUPerson * > & p)
{
	cout << *c << endl;
	for(int i = 0; i < p.size(); i++)
	{
		if(p[i]->GetClassType() < 3){
		
			Student * s  = dynamic_cast<Student *>(p[i]);
			vector <Course *> cr = s->GetCourses();
			for(int j = 0; j < cr.size(); j++)
			{
				if(cr.size() > 0 &&  cr[j] != NULL && c->GetID() == cr[j]->GetID())
					if(s != NULL)
						cout << s->GetName() << endl;
			}
		}
	}
}

void PrintAReportOfEmployeeSalaries (vector <SSUPerson * > & p)
{
	// Create a vector of department types
	vector <string> departments;
	// Store all employees
	vector <Employee *> emps;
	cout << "Gathering Departments ";
	// Go through each person
	for(int i = 0; i < p.size(); i++)
	{
		cout  << "." ;
		// Checks if they are an employee
		if(p[i]->GetClassType() >2)
		{
			Employee * tmp = dynamic_cast<Employee *>(p[i]);
			string d = tmp->GetDepartment();
			// Keeps track of weather or not this department has already been added
			bool alreadyInDepartments = false;
			// Look through and find new departments.
			for(int j = 0; j < departments.size(); j++)
			{
				// If the department matches one already in the vector,
				// stop looking.
				if(departments[j] == d){
					alreadyInDepartments = true;
					break;
				}
			}
			// If the loop made it through, that means the department was not found
			// Add it to the list of departments.
			if(!alreadyInDepartments)
				departments.push_back(d);
			// Save this employee.
			emps.push_back(tmp);
		}
	}
	cout << " Found " << departments.size() << " departments.";
	cout << endl;

	// Sort the employees by ID (This doesn't seem necessary, it appears they are coming in a sorted manner.
	
	// This would be more efficient by removing the employee after we printed them, but meh, that requires writing the 
	// loop entierly differently.
	
	// for each department type
	for(int i = 0; i < departments.size(); i++)
	{
		// To keep track of totals
		int totalSalary = 0;
		// write a report for each person in this department
		cout << "\n======== Report for " << departments[i] << endl;
		// Go through each employee and see if they are part of this department.
		for(int j = 0; j < emps.size(); j++)
		{
			if(emps[j]->GetDepartment() == departments[i])
			{
				totalSalary += emps[j]->GetSalary();
				cout <<  emps[j]->GetID() <<"\t$" << emps[j]->GetSalary() << "\t" << emps[j]->GetName() <<   endl;

			}
		}
		// Print out the total for this department
		cout << "Total Salary: \t$" << totalSalary << endl << endl;
	}
}

void PrintATeamListForASport ()
{
	cout << "Function for option j. Print a Team List for a Sport\n";
}

void PrintAListOfStudentsInGPARange ()
{
	cout << "Function for option k. Print a List of Students in a GPA Range\n";
}

void PrintAStudentsSchedule()
{
	cout << "Function for option l. Print a Student's Schedule\n";
}

void CancelLowEnrolledCourses ()
{
	cout << "Function for option m. Cancel Low Enrolled Courses\n";
}

void PutStudentsOnProbation ()
{
	cout << "Function for option n. Put Students on Probation\n";
}

