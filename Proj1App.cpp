#include <iostream>
#include <vector>
#include <string>
#include <cstdlib>
//#include "SSU.h" // Causes double include, and the enum is outside the double include check.
//#include "Proj1RW.h"
#include "Proj1Functions.h"
#include "Proj1RW.h"

using namespace std;

char Menu ();

int main (int argc, char * argv [])
{
	string inputPfile = "SSUPeopleInput.txt";
	string inputCfile = "SSUCoursesInput.txt";
	string outputPfile = "SSUPeopleOutput.txt";
	string outputCfile = "SSUCoursesOutput.txt";
	for (int i = 1; i < argc; i++)
	{
		if (string(argv[i]) == "-ip")
			inputPfile = argv[++i];
		else if (string(argv[i]) == "-ic")
			inputCfile = argv[++i];
		else if (string(argv[i]) == "-op")
			outputPfile = argv[++i];
		else if (string(argv[i]) == "-oc")
			outputCfile = argv[++i];
	}

	vector <SSUPerson *> People;
	vector <Course *> Courses;
	ReadFiles (inputCfile, Courses, inputPfile, People);
	cout << People.size() << " people loaded from " << inputPfile << " and "
	     << Courses.size() << " courses loaded from " << inputCfile << ".\n\n";
	
	/*cout << "NameTest: " << People[0]->GetName() << endl;	
	Student * tmp = dynamic_cast<Student *>(People[0]);
	cout << "NameTest2: " << tmp->GetName() << endl;
	*/
	char choice;
	do
	{
		choice = Menu ();
		int i;
		int tmp;
		int ID = -1;
		string id;
		string name;
		string type;
		switch (choice)
		{
			case 'a': 
				cout << "Enter ID: ";
				cin >> ID;
				cout << endl;
				
                                cout << "Enter Name: ";
                                cin >> name;
                                cout << endl;

                                cout << "Enter Type: ";
                                cin >> type;
                                cout << endl;
					
				AddAPerson (People, ID, name, type); 
				break;
			case 'b': DeleteAPerson (People, Courses); break;
			case 'c': AddACourse (); break;
			case 'd': DeleteACourse (); break;
			case 'e': AddAStudentToACourse (); break;
			case 'f': DropAStudentFromACourse (); break;
			case 'g': ModifyAnExistingPerson (); break;
			case 'h':
				cout << "Enter ID: ";
                                cin >> id;
                                cout << endl;

				for(i = 0; i < Courses.size(); i++)
				{
					if(Courses[i]->GetID() == id)
						ID = i;
				}		
				if(ID > -1)
				 	PrintARosterForACourse (Courses[ID], People); break;
			case 'i': PrintAReportOfEmployeeSalaries (People); break;
			case 'j': PrintATeamListForASport (); break;
			case 'k': PrintAListOfStudentsInGPARange (); break;
			case 'l': PrintAStudentsSchedule(); break;
			case 'm': CancelLowEnrolledCourses (); break;
			case 'n': PutStudentsOnProbation (); break;
		}
	} while (choice != 'q' && choice != 'Q');

	WriteFiles (outputCfile, Courses, outputPfile, People);
	cout << "\nModified files have been saved as " << outputPfile
		<< " and " << outputCfile << ".\n\n";

	return 0;
}

char Menu ()
{
	cout << "\tWelcome to CS360's People/Course Database System\n\n";
	cout << "Please select one of the following menu options:\n\n";
	cout << "\ta. Add a Person\n";
	cout << "\tb. Delete a Person\n";
	cout << "\tc. Add a Course\n";
	cout << "\td. Delete a Course\n";
	cout << "\te. Add a Student to a Course\n";
	cout << "\tf. Drop a Student from a Course\n";
	cout << "\tg. Modify an Existing Student\n";
	cout << "\th. Print a Roster for a Course\n";
	cout << "\ti. Print a Report of Employee Salaries\n";
	cout << "\tj. Print a Team List for a Sport\n";
	cout << "\tk. Print a List of Students in a GPA Range\n";
	cout << "\tl. Print a Student's Schedule\n";
	cout << "\tm. Cancel Low Enrolled Courses\n";
	cout << "\tn. Put Students on Probation\n";
	cout << "\tq. Quit\n\n";
	cout << "Enter the selection letter: ";
	char selection = 'q';
	cin >> selection;
	return selection;
}
