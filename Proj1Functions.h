#include <vector>
#include "SSU.h"
#include "Proj1RW.h"

void AddAPerson (vector <SSUPerson * > & p, int ID, string name, string type);
bool DeleteAPerson (vector <SSUPerson * > & p, vector <Course * > & c);
void AddACourse ();
void DeleteACourse ();
void AddAStudentToACourse ();
void DropAStudentFromACourse ();
void ModifyAnExistingPerson ();
void PrintARosterForACourse (Course * & c, vector <SSUPerson * > & p);
void PrintAReportOfEmployeeSalaries (vector <SSUPerson * > & p);
void PrintATeamListForASport ();
void PrintAListOfStudentsInGPARange ();
void PrintAStudentsSchedule();
void CancelLowEnrolledCourses ();
void PutStudentsOnProbation ();
