#include <iostream>
#include <iomanip>
#include <vector>
#include <map>
#include <string>
#include <sstream>
#include <cstdlib>

enum CLASS_TYPE {PERSON, STUDENT, ATHLETE, EMPLOYEE, STAFF, FACULTY, STUDENTEMPLOYEE};  

class Course;

#ifndef SSUPERSON_H
#define SSUPERSON_H
// File: SSUPerson.h
// Contents: This file contains the desciption of a class called SSUPerson.

using namespace std;
class SSUPerson
{
 public:
// The GetClassType function will return a value from the enumerated type CLASS_TYPE
// corresponding the the instantiated class type. This function is overridden
// by all subclasses.
    virtual CLASS_TYPE GetClassType () const;
// The Read function will read a formatted description of the class from the opened
// input stream passed to it and store the data in the SSU_Person object. This
// function is overridden by all subclasses.
    virtual string Read (istream & ins);
// The Write function will a write the data in the SSU_Person object in a
// formatted manner to the opened output stream passed to it. This
// function is overridden by all subclasses.
    virtual void Write (ostream & outs) const;
// This Link function is overridden by the Student and Faculty classes to link the
// Course * entries in the map to the Course vector in an instantiated Student
// or Faculty object.
    virtual void Link (const string & info, const map <string, Course *> & Cmap);
// This Link function is overridden by the Staff class to link the SSUPerson *
// entries in the map to the supervisor attribute in an instantiated Staff object.
    virtual void Link (const string & info, const map <string, SSUPerson *> & Pmap);
  // The default constructor will set name to "", ID to 0 and email "" 
  SSUPerson ();
  // This constructor will set name, ID and email to the values passed.
  SSUPerson (string N, int IDnum, string E);
  // This mutator function will set name to the value passed.
  void SetName(string N);
  // This accessor function will return the name of the person.
  string GetName () const;
  // This function will set ID to the value passed.
  void SetID(int IDnum);
  // This function will return the ID of the person.
  int GetID() const;
  // This function will set email to the value passed.
  void SetEmail(string E);
  // This function will return the email of the person.
  string GetEmail() const;
  // This function will return the object type.
  virtual string type() const;
  // The output operator will write out the person
  friend ostream & operator << (ostream & outs, const SSUPerson & P);
  // The less than operator will compare the ID of the students
  friend bool operator < (SSUPerson &P, SSUPerson &P2);

 protected:
  string name;
  int ID;
  string eMail;
};
#endif



// File:  Student.h
// Description: This file contains the description of the class called Studeent.

#ifndef STUDENT_H
#define STUDENT_H

using namespace std;

class Course;

class Student : virtual public SSUPerson {
public:
    CLASS_TYPE GetClassType () const;
    string Read (istream & ins);
    void Write (ostream & outs) const;
    void Link (const string & info, const map <string, Course *> & Cmap);

    // The default constructor will set major to "", currentUnits to 0, unitsCompleted to 0, GPA to 0.0, tuition to 0.0, balance to 0.0, and courses to NULL.
    Student ();
    // This constructor will set major, currentUnits, unitsCompleted, GPA, tuition, balance, and courses to the values passed.
    Student (string major1, int currentUnits1, int unitsCompleted1, float GPA1, float tuition1, float balance1, vector<Course *> courses1, string name1, int ID1, string eMail1);
    // The copy constructor will make a copy of Student.
    Student (Student & student); 
    // This mutator function will set major to the value passed.
    void SetMajor (string major2);
    // This mutator function will set currentUnits to the value passed.
    void SetCurrentUnits (int currentUnits2);
    // This mutator function will set unitsCompleted to the value passed.
    void SetUnitsCompleted (int unitsCompleted2);
    // This mutator function will set GPA to the value passed.
    void SetGPA (float GPA2);
    // This mutator function will set tuition to the value passed.
    void SetTuition (float tuition2);
    // This mutator function will set balance to the value passed.
    void SetBalance (float balance2);
    // This mutator function will set courses to the value(s) passed.
    void SetCourses (vector<Course *> courses2);
    // This accessor function will return the value of major.
    string GetMajor () const;
    // This accessor function will return the value of currentUnits.
    int GetCurrentUnits () const;
    // This accessor function will return the value of unitsCompleted.
    int GetUnitsCompleted () const;
    // This accessor function will return the value of GPA.
    float GetGPA () const;
    // This accessor function will return the value of tuition.
    float GetTuition () const;
    // This accessor function will return the value of balance.
    float GetBalance () const;
    // This accessor function will return the vector courses.
    vector<Course *> GetCourses () const;
    // This function will return the value "Student" - the name of the class.
    string type () const;
protected:
    // The major of the student.
    string major;
    // The number of units the student is taking in the current semester.
    int currentUnits;
    // The number of units the student has already completed.
    int unitsCompleted;
    // The student's grade point average.
    float GPA;
    // The amount of money the student must pay to the school for the classes.
    float tuition;
    // The amount of money that the student owes the school.
    float balance;
    // The classes that that student is currently enrolled in.
    vector <Course *> courses;
};

#endif


#ifndef ATHLETE_H
#define ATHLETE_H
// File: Athlete.h
// Contents: This file contains the desciption of a class called Athlete.


using namespace std;

class Course;

class Athlete : public Student
{
 public:
    CLASS_TYPE GetClassType () const;
    string Read (istream & ins);
    void Write (ostream & outs) const;

  // The default constructor will set sport to ""
  Athlete ();
  // This constructor will set sport and scholarship to the values passed.
  Athlete (string s, float sship, string n, int IDnum, string e, string m, int current, int complete, float gpa, float t, float b, vector <Course*> c);
  // This function will set sport to the value passed.
  void SetSport(string sport);
  // This function will return the sport of the person.
  string GetSport() const;
  // This function will set scholarship to the value passed.
  void SetScholarship(float sship);
  // This function will return the scholarship of the person.
  float GetScholarship() const;
  // This function will return the object type
  string type() const;
 private:
  string sport;
  float scholarship;
};

#endif







#ifndef EMPLOYEE_H
#define EMPLOYEE_H

using namespace std;

class Employee : virtual public SSUPerson{
 public:
    CLASS_TYPE GetClassType () const;
    string Read (istream & ins);
    void Write (ostream & outs) const;

  //The default constructor will set the department to "", years to 0, salary to 0.0, and title to ""
  Employee();
  //This constructor will set the department, years, salary, and title to the values passed
  Employee(string d, int y, float s, string t, string n, int IDnum, string e);
  //This mutator function will set the department to the string passed
  void SetDepartment(string d);
  //This accessor function will return the department
  string GetDepartment();
  //This mutator function will set the years to the int passed
  void SetYears(int y);
  //This accessor function will return the years
  int GetYears();
  //This mutator function will set the salary to the float passed
  void SetSalary(float s);
  //This accessor function will return the salary
  float GetSalary();
  //This mutator function will set the title to the string passed
  void SetTitle(string t);
  //This accessor function will return the title
  string GetTitle();
  // This function will return the object type
  string type() const;
 protected:
  //The department the employee works for
  string department;
  //The years the employee has worked for
  int years;
  //The salary of the employee
  float salary;
  //The employee's title
  string title;
};

#endif
#ifndef FACULTY_H
#define FACULTY_H


using namespace std;

class Course;

class Faculty : public Employee{
 public:
    CLASS_TYPE GetClassType () const;
    string Read (istream & ins);
    void Write (ostream & outs) const;
    void Link (const string & info, const map <string, Course *> & Cmap);

  //The default constructor will set tenured to 0, units to 0.0, and will create a Courses vector
  Faculty();
  //This constructor will set tenured, units, and courses to the values passed
  Faculty(bool t, float u, vector <Course *> c, string d, int y, float sal, string ttle, string nam, int id, string em);
  //This mutator function will set the tenured to the int passed
  void SetTenured(bool t);
  //This accessor function will return 1 if tenured, 0 if not
  bool isTenured();
  //This mutator function will set units to the float passed
  void SetUnits(float u);
  //This accessor function will return the units
  float GetUnits();
  //This mutator function will set the courses 
  void SetCourses(vector <Course *> c);
  //This accessor function will return courses
  vector <Course*> GetCourses();
  // This function will return the object type
  string type() const;
 protected:
  //Weather or not the faculty is tenured
  bool tenured;
  //How many units the faculty is teaching
  float units;
  //what courses the faculty is teaching
  vector <Course *> courses;
};

#endif
#ifndef STAFF_H
#define STAFF_H
// File: Staff.h
// Contents: This file contains the desciption of a class called Staff.

using namespace std;
class Staff : public Employee
{
 public:
    CLASS_TYPE GetClassType () const;
    string Read (istream & ins);
    void Write (ostream & outs) const;
    void Link (const string & info, const map <string, SSUPerson *> & Pmap);

  // The default constructor will set hours to 0 and supervisorID
  Staff ();
  // This constructor will set hours and supervisorID to the values passed
  Staff (int h, SSUPerson * s, string d, int y, float sal, string ttle, string nam, int id, string em);
  // This mutator function will set hours to the value passed.
  void SetHours(int hours);
  // This accessor function will return the hours of the person.
  int GetHours () const;
  // This function will set supervisor to the value passed.
  void SetSupervisor(SSUPerson * s);
  // This function will return the supervisor of the person.
  SSUPerson * GetSupervisor() const;
  // This function will return the object type.
  string type() const;

 protected:
  int hours;
  SSUPerson * supervisor;
};
#endif
#ifndef STUDENTEMPLOYEE_H
#define STUDENTEMPLOYEE_H
// File: StudentEmployee.h
// Contents: This file contains the desciption of a class called StudentEmployee.

using namespace std;
class StudentEmployee : public Staff, public Student
{
 public:
    CLASS_TYPE GetClassType () const;
    string Read (istream & ins);
    void Write (ostream & outs) const;

  // The default constructor will set maxHours to 0
  StudentEmployee ();
  // This copy constructor will set the student employee to the pass value
  StudentEmployee ( StudentEmployee & StudentEmp);
  // This constructor will set maxHours to the value passed.
  StudentEmployee (int maxHours1, string major1, int currentUnits1, int unitsCompleted1, float GPA1,
		   float tuition1, float balance1, vector<Course *> courses1,
		   int hours1, Employee* supervisor1,
		   string department1, int years1, float salary1, string title1,
		   string name1, int ID1, string eMail1);
  // This function will set maxHours to the value passed.
  void SetMaxHours(int maxHours);
  // This function will return the maxHours of the person.
  int GetMaxHours() const;
  // This function will return the object type.
  string type() const;

 private:
  int maxHours;
};
#endif
#ifndef COURSE_H
#define COURSE_H
// File: Course.h
// Contents: This file contains the desciption of a class called Course.

using namespace std;

class Faculty;

class Course
{
 public:
    CLASS_TYPE GetClassType () const;
    string Read (istream & ins);
    void Write (ostream & outs) const;
    void Link (const string & info, const map <string, SSUPerson *> & Pmap);
    int GetUnits () const;
    void SetUnits (int U);

  // The default constructor will set name to "", ID to 0 and enrollment ""
  Course ();
  // This constructor will set name, ID, enrollment, location, time and instructor to the values passed.
  Course (string name, string ID, int enrollment, string location, string time, SSUPerson * instructor);
  // This mutator function will set name to the value passed.
  void SetName(string name);
  // This accessor function will return the name of the course.
  string GetName () const;
  // This function will set ID to the value passed.
  void SetID(string ID);
  // This function will return the ID of the course.
  string GetID() const;
  // This function will set enrollment to the value passed.
  void SetEnrollment(int enrollment);
  // This function will return the enrollment of the course.
  int GetEnrollment() const;
  // This function will set location to the value passed.
  void SetLocation(string location);
  // This function will return the location of the course.
  string GetLocation() const;
  // This function will set time to the value passed.
  void SetTime(string time);
  // This function will return the time of the course.
  string GetTime() const;
  // This function will set instructor to the value passed.
  void SetInstructor(SSUPerson * instructor);
  // This function will return instructor.
  SSUPerson * GetInstructor() const;
  // This overloaded output operator will print out the course
  friend ostream & operator << (ostream &outs, const Course & C);
  // This overloaded less than operator will return true if left Course is less than right 
  friend bool operator < (const Course & left, const Course & right);

 private:
  string name;
  string ID;
  int enrollment;
  string location;
  string time;
  SSUPerson * instructor;
	int units;
};
#endif
